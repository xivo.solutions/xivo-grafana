# Grafana - Create Xuc calls graph 

## Purpose

The aim of the project is to create a graphs about the audio quality issues of the users thanks to the data that we can find in logs.

Fluentd is a tools that allow us to parse logs.
Currently we use fluentd (Project : xivocc-fluentd ) to parse XUC logs to retreive call quality issues data (ie: RTT, jitter UP/DOWN, Packet loss UP/DOWN)
Then fluentd pushes the data into xivo_db and thanks to Grafana we are able to dipslay graphs.

[ diagram ]

## Debug / Test

1. Start the docker container with docker-compose:
```
cd xivo-grafana
ENV XIVO_HOST is mandatory
docker-compose up -d
```
1. Login in grafana
```
http://<xivopbx_ip>:3000/
```
