FROM grafana/grafana-oss:9.4.7-ubuntu

COPY ./grafana/provisioning /etc/grafana/provisioning
# COPY ./grafana/grafana.ini /etc/grafana/grafana.ini

ARG TARGET_VERSION
LABEL version=${TARGET_VERSION}

